require('dotenv').config();
const express = require('express');
const cors = require('cors');

const routerApi = require('./routes');
const { dbConnection } = require('./db/conn');

const app = express();
const port = process.env.PORT || 8080;

async function connect(){
  await dbConnection();
}
connect();

app.use(cors());
app.use(express.json());

routerApi(app);

// iniciamos nuestro servidor
app.listen(port, () => {
  console.log('API escuchando en el puerto ' + port)
});