const { Schema, model } = require('mongoose');

const ProductSchema = Schema({
  nombre: {
    type: String,
    unique: true,
    require: [true, 'Nombre requerido']
  },
  cantidad: {
    type: Number,
    require: true,
  },
  precio: {
    type: Number,
    default: 0,
  }
});

module.exports = model('Product', ProductSchema);