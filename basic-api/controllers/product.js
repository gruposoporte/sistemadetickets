const getProducts = async (req, res) => {
  res.json({
    msg: 'Controller get products'
  });
}

const getProduct = async (req, res) => {
  const { id } = req.params;

  res.json({
    id,
    msg: 'Controller get products'
  });
}

const createProduct = async (req, res) => {
  res.json({
    msg: 'Controller create product'
  });
}

const updateProduct = async (req, res) => {
  const { id } = req.params;

  res.json({
    id,
    msg: 'Controller update product'
  });
}

const deleteProduct = async (req, res) => {
  const { id } = req.params;

  res.json({
    id,
    msg: 'Controller delete product'
  });
}

module.exports = {
  getProducts,
  getProduct,
  createProduct,
  updateProduct,
  deleteProduct
}