const express = require('express');

const products = require('./product');

function routerApi(app) {
  const router = express.Router();
  app.use('/api', router);
  router.use('/products', products);
}

module.exports = routerApi;